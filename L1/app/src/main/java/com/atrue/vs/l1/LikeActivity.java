package com.atrue.vs.l1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class LikeActivity extends AppCompatActivity {

    private ListView list;
    private String [] test = new String[] {"Фильм 1", "Фильм 2", "Фильм 3"};
    private Button searchFilms;
    private EditText searchValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like);
        list();
    }

    public void list() {
        list = (ListView) findViewById(R.id.films);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.films, test);
        list.setAdapter(adapter);

        list.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String val = (String) list.getItemAtPosition(position);
                        Toast.makeText(
                                LikeActivity.this,
                                "Описание",
                                Toast.LENGTH_SHORT
                        ).show();
                    }
                }
        );

        searchValue = (EditText) findViewById(R.id.searchValue);
        searchFilms = (Button) findViewById(R.id.searchFilms);
        searchFilms.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                            Toast.makeText(
                                    LikeActivity.this, searchValue.getText(),
                                    Toast.LENGTH_SHORT
                            ).show();
                    }
                }
        );

    }
}
