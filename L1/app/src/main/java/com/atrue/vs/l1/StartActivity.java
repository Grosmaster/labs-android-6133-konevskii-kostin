package com.atrue.vs.l1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {

    private EditText password;
    private EditText phone;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        addListenerOnButton();
    }

    public void addListenerOnButton(){

        password = (EditText) findViewById(R.id.password);
        phone = (EditText) findViewById(R.id.phone);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(password.getText().toString().equals("1") && phone.getText().toString().equals("1")) {
                            Intent intent = new Intent(".MainNGDActivity");
                            startActivity(intent);
                        }
                        else {
                                Toast.makeText(
                                        StartActivity.this, "Не привильный пароль или телефон",
                                Toast.LENGTH_SHORT
                        ).show();
                        }
                    }
                }
        );

    }
}
