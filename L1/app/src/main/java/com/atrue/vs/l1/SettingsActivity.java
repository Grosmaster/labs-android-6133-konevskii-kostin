package com.atrue.vs.l1;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsActivity extends AppCompatActivity {

    final int MENU_COLOR_RED = 1;
    final int MENU_COLOR_GREEN = 2;
    final int MENU_COLOR_BLUE = 3;

    private EditText password;
    private EditText date;
    private Button save;
    private TextView tvColor;
    String[] data = {"Все", "Боевик", "Вестерн", "Комедия", "Мелодрама"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setTitle("TabHost");

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);

        tabHost.setup();

        TabHost.TabSpec tabSpec = tabHost.newTabSpec("tag1");

        tabSpec.setContent(R.id.tab1);
        tabSpec.setIndicator("Профиль");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("tag2");
        tabSpec.setContent(R.id.tab2);
        tabSpec.setIndicator("Общие");
        tabHost.addTab(tabSpec);

        tabHost.setCurrentTab(0);
        addListenerOnButton();

        // адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        // заголовок
        spinner.setPrompt("Жанры");
        // выделяем элемент
        spinner.setSelection(2);
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // показываем позиция нажатого элемента
                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        tvColor = (TextView) findViewById(R.id.textColor);
        registerForContextMenu(tvColor);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.textColor:
                menu.add(0, MENU_COLOR_RED, 0, "Красный");
                menu.add(0, MENU_COLOR_GREEN, 0, "Зеленый");
                menu.add(0, MENU_COLOR_BLUE, 0, "Синий");
                break;
        }


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            // пункты меню для tvColor
            case MENU_COLOR_RED:
                tvColor.setTextColor(Color.RED);

                break;
            case MENU_COLOR_GREEN:
                tvColor.setTextColor(Color.GREEN);

                break;
            case MENU_COLOR_BLUE:
                tvColor.setTextColor(Color.BLUE);

                break;
        }
        return super.onContextItemSelected(item);
    }

    public void addListenerOnButton(){

        password = (EditText) findViewById(R.id.passwordEdit);
        date = (EditText) findViewById(R.id.date);
        save = (Button) findViewById(R.id.save);
        save.setOnClickListener(

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
                        builder.setMessage("Вы уверны?")
                                .setCancelable(false)
                                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(
                                                SettingsActivity.this,
                                                "Сохранено",
                                                Toast.LENGTH_SHORT);
                                    }
                                }).setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                Toast.makeText(
                                        SettingsActivity.this,
                                        "Отмена",
                                        Toast.LENGTH_SHORT);
                            }
                        });
                        AlertDialog alert = builder.create();
                        alert.setTitle("Сохранить");
                        alert.show();
                    }
                }
        );

    }
}
